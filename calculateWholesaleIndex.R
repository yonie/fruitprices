library(lubridate)

rawWholesaleFruit <- merge(rawWholesaleFruit, CBSWholesaleMap, by="identifier")[,c("identifier","date"
                                                                                   ,"price","cbsNum")]
# TODO: I need to iterate over distinctFruitCategoriesCBS$nums. We'll take the first
# (oranges) for simplicity then go on

wholesaleFruitPriceIndex <-
  data.frame(c((11:12), rep(1:12, 3)), c(rep(2015, 2),
                                         rep(2016, 12), rep(2017, 12), rep(2018, 12)))
names(wholesaleFruitPriceIndex) <- c("month", "year")


for(fruities in 1:14){
  curFruit <- distinctFruitCategoriesCBS$nums[fruities]
  tempFruitPriceIndex <-
    data.frame(c((11:12), rep(1:12, 3)), c(rep(2015, 2),
                                           rep(2016, 12), rep(2017, 12), rep(2018, 12)),rep(NA,38))
  names(tempFruitPriceIndex) <- c("month", "year",curFruit)
  normalator = 0
  for(j in 2015:2018){
    for(i in 1:12){
      if(i < 11 & j == 2015)
        next()
      # print(paste0("j ",j," i ",i))
      curMonthsFruit <- rawWholesaleFruit[rawWholesaleFruit$cbsNum == curFruit & 
                                            month(ymd(rawWholesaleFruit$date)) == i & 
                                            year(ymd(rawWholesaleFruit$date)) == j,]
      if(nrow(curMonthsFruit) > 0 & normalator == 0)
        normalator = 100/mean(curMonthsFruit$price)
      tempFruitPriceIndex[[toString(curFruit)]][(j-2015)*12+i-10] <-
        mean(curMonthsFruit$price)*normalator
      # 
      
    }
  }
  #wholesaleFruitPriceIndex <- join(wholesaleFruitPriceIndex,tempFruitPriceIndex)
  print(paste0("Adding wholesale ",names(tempFruitPriceIndex)[3] ))
  wholesaleFruitPriceIndex <- cbind.data.frame(wholesaleFruitPriceIndex,tempFruitPriceIndex[,3])
  names(wholesaleFruitPriceIndex)[length(names(wholesaleFruitPriceIndex))] <- names(tempFruitPriceIndex)[3]
  
}

rm(list=c("tempFruitPriceIndex", "curFruit", "fruities", "i", "j", "normalator", "curMonthsFruit"))
