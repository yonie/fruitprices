goodforRatio <- data.frame(date = CBSFruitLevels$date)

# Creating the table of the values that can participate in the ratio
# calculation used for 
for(j in 2:ncol(CBSFruitLevels)){
  curFruit = data.frame(cur = numeric(48))
  names(curFruit) <- names(CBSFruitLevels[j])
  for(i in 2:nrow(CBSFruitLevels)){
    if(!is.na(CBSFruitLevels[i,j]) & !is.na(CBSFruitLevels[i-1,j]))
      curFruit[i,1] = 1
    else{
      curFruit[i,1] = 0
      #print(paste0("Inserting null on ", is.na(CBSFruitLevels[i,j]), 
      #    " and ", is.na(CBSFruitLevels[i-1,j]), " i is ", i)) 
    }
  }
  goodforRatio <- cbind(goodforRatio, curFruit)
}

rm(list = "curFruit", "i", "j")

# *** Magic Number ***
totalFruitWeight = 9.51
fixedWeights <- data.frame(rep(7,14))
names(fixedWeights) <- c("fixedWeight")

for(i in 1:14){
  fixedWeights$fixedWeight[i] <- 
    CBSFruitWeights$weight[i]*totalFruitWeight/sum(CBSFruitWeights$weight)
}

CBSFruitWeights <- cbind(CBSFruitWeights,fixedWeights)
rm(list = "fixedWeights")

ratios <- rep(NA,48)

# Calculating ratios 
for(j in 2:48){
  num = 0
  den = 0
  for(i in 2:(ncol(CBSFruitLevels)-1)){
    weight = CBSFruitWeights[CBSFruitWeights$ID == names(CBSFruitLevels[i]), 4]
    if(goodforRatio[j,i]){
      num = num + weight*CBSFruitLevels[j,i]/CBSFruitLevels[j-1,i]
      den = den + weight
    }
  }
  ratios[j] = num/den
}

rm(list=c("num","den","i","j","totalFruitWeight","weight"))


completedCBSFruitLevels <- CBSFruitLevels[0,]

for(i in 1:ncol(completedCBSFruitLevels)){
  completedCBSFruitLevels[1,i] <- CBSFruitLevels[1,i]
}


for(j in 2:48){
  completedCBSFruitLevels[nrow(completedCBSFruitLevels)+1,] <- NA
  completedCBSFruitLevels[nrow(completedCBSFruitLevels),1] <- 
    CBSFruitLevels[nrow(completedCBSFruitLevels),1]
  for(i in 1:ncol(completedCBSFruitLevels)){
    if(!is.na(CBSFruitLevels[j,i]) | i == ncol(completedCBSFruitLevels))
      completedCBSFruitLevels[j,i] <- CBSFruitLevels[j,i]
    else
      completedCBSFruitLevels[j,i] <- completedCBSFruitLevels[j-1,i]*ratios[j]
  }
}

# Cleaning the headline CBS figures from the prices*weights that we know of
# This will give a residual that we can clean with the lasso or something
cleanCBSFruit <- rep(NA, 48)

completedCBSFruitLevels$CBSFruitIndexClean = completedCBSFruitLevels$CBSFruitIndex

for(i in 1:5){
  completedCBSFruitLevels$CBSFruitIndexClean[i] <- NA
}

for(j in 6:48){
  for(i in 2:15){
    completedCBSFruitLevels$CBSFruitIndexClean[j] = 
      completedCBSFruitLevels$CBSFruitIndexClean[j] - 
      CBSFruitWeights[CBSFruitWeights$ID==names(completedCBSFruitLevels)[i],4]/
      sum(CBSFruitWeights[,4])*completedCBSFruitLevels[j,i]
  }
}

# Now we can lasso CBSFruitIndexClean over 2:15 columns of completedCBSFruitLevels
completedCBSFruitLevels <- completedCBSFruitLevels[c(-(1:5),-(45:48)),]

x <- model.matrix(CBSFruitIndexClean~., completedCBSFruitLevels[,-c(1,16)])[,-1]
y <- completedCBSFruitLevels$CBSFruitIndexClean

#glmnet(x,y, alpha=1, lambda=.5)
remainderModel <- lm(y~x[,c(1,12,14)])

CBSFruitWeights[nrow(CBSFruitWeights) + 1,] = rep(NA,4)
CBSFruitWeights[,5] = 0
CBSFruitWeights$name[15] <- "intercept"
names(CBSFruitWeights)[5] <- "LMCoefficients"

CBSFruitWeights[CBSFruitWeights$name == "intercept",]$LMCoefficients <- coefficients(remainderModel)[1] 
CBSFruitWeights[CBSFruitWeights$ID == "150210" & !is.na(CBSFruitWeights$ID),]$LMCoefficients <- coefficients(remainderModel)[2]
CBSFruitWeights[CBSFruitWeights$ID == "150220" & !is.na(CBSFruitWeights$ID),]$LMCoefficients <- coefficients(remainderModel)[3]
CBSFruitWeights[CBSFruitWeights$ID == "150265" & !is.na(CBSFruitWeights$ID),]$LMCoefficients <- coefficients(remainderModel)[4]

#setwd("/home/yaelg/pub/Yael/temp")
#write.csv(CBSFruitWeights, file = "CBSFruitWeights",row.names=FALSE)



CBSfrequencyMap <- data.frame(date=1:12)

tempList <- rep(0,12)
CBSFruitLevelsTrimmed = CBSFruitLevels[-c(44:48),] 

for(j in 2:(ncol(CBSFruitLevels) - 1)){
  for(i in 1:12){
    tempList[i] <- sum(!is.na(CBSFruitLevelsTrimmed[month(CBSFruitLevelsTrimmed$date) == i ,j]))/
      length(CBSFruitLevelsTrimmed[month(CBSFruitLevelsTrimmed$date) == i,j])
  }
  CBSfrequencyMap <- cbind(CBSfrequencyMap, tempList)
  names(CBSfrequencyMap)[ncol(CBSfrequencyMap)] <- names(CBSFruitLevels)[j]
}

# Now we'll take the latest CBS print as base and start reiterating over that 
# ---until the lastMonth--- for one month for starters 
# First we take the observations that need to come from the data, conditioned that we /have/
# the data for them and that they have a high enough probability for being polled (and 
# not extrapolated) by the CBS

lastCBSMonth = max(as.Date(CBSFruitLevels[!is.na(CBSFruitLevels$CBSFruitIndex),1], "%Y-%m-%d")) %m+% months(1)
print(paste0("Extrapolating for ", lastCBSMonth))
completedCBSFruitLevels[nrow(completedCBSFruitLevels) +1 ,1] <- as.character(lastCBSMonth)
row.names(completedCBSFruitLevels)[nrow(completedCBSFruitLevels)] <- "Est"

for(j in 2:15){
  if (CBSfrequencyMap[CBSfrequencyMap$date == month(lastCBSMonth),j] > CBSfrequencyCutoff)
    completedCBSFruitLevels[nrow(completedCBSFruitLevels),j] = estimatedSubseries[nrow(estimatedSubseries),j]
  
}

# TODO: Complete build ratios and extrapolate from previous prints with that
# Now we'll need to calculate the expected change at the prices of the fruit that
# "didn't change." A reasonable approach can be to caculate an index for the pre
# and post periods, wsing the weights of each vegetable, then taking the, erm, ratio
# of the two.
preIndex = 0
postIndex = 0

for(j in 2:15){
  if(!is.na(CBSFruitLevels[CBSFruitLevels$date == lastCBSMonth %m-% months(1),j]) &
  !is.na(completedCBSFruitLevels[completedCBSFruitLevels$date == lastCBSMonth,j])){
    preIndex = preIndex + CBSFruitLevels[CBSFruitLevels$date == lastCBSMonth %m-% months(1),j]*CBSFruitWeights$weight[j-1]
    postIndex = postIndex + estimatedSubseries[as.character(estimatedSubseries$Date) == lastCBSMonth,j]*CBSFruitWeights$weight[j-1]
  }
}

ratio = postIndex/preIndex


for(j in 2:15){
  if(is.na(completedCBSFruitLevels[completedCBSFruitLevels$date == lastCBSMonth,j])){
    completedCBSFruitLevels[completedCBSFruitLevels$date == lastCBSMonth,j] = completedCBSFruitLevels[completedCBSFruitLevels$date == lastCBSMonth %m-% months(1),j]*ratio
  }
}
# Building headline estimats from the subseries. Both for (imprefect) cross validation
# as well as for estimation of new headline datum

completedCBSFruitLevels[ncol(completedCBSFruitLevels) + 1] <- rep(0, nrow(completedCBSFruitLevels))
names(completedCBSFruitLevels)[length(completedCBSFruitLevels)] <- "CBSFRuitIndexHat"

for(j in 1:nrow(completedCBSFruitLevels)){
  for(i in 2:15){
    completedCBSFruitLevels$CBSFRuitIndexHat[j] = 
      completedCBSFruitLevels$CBSFRuitIndexHat[j] + 
      (CBSFruitWeights[!is.na(CBSFruitWeights$ID) & CBSFruitWeights$ID==names(completedCBSFruitLevels)[i],4]/
         sum(CBSFruitWeights[!is.na(CBSFruitWeights[,4]),4]) + CBSFruitWeights[!is.na(CBSFruitWeights$ID) & CBSFruitWeights$ID==names(completedCBSFruitLevels)[i],5])*completedCBSFruitLevels[j,i]
  }
  completedCBSFruitLevels$CBSFRuitIndexHat[j] = 
    completedCBSFruitLevels$CBSFRuitIndexHat[j] + coefficients(remainderModel)[1]
}



# You can validate the model with this:
sum(completedCBSFruitLevels$CBSFRuitIndexHat - completedCBSFruitLevels$CBSFruitIndex)

rm(list=c("x","y","i","j","ratios","cleanCBSFruit","CBSFruitLevelsTrimmed","tempList"))
